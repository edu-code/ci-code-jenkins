pipeline {
    agent {
        docker {
            image 'python:3.7.2'
        }
    }
    
    triggers {
        pollSCM('H/5 * * * 1-5')
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '30', artifactNumToKeepStr: '30'))
        // Configure an overall timeout for the build of one hour.
        timeout(time: 1, unit: 'HOURS')
        // When we have test-fails e.g. we don't need to run the remaining steps
        skipStagesAfterUnstable()
        
        ansiColor('xterm')
        timestamps()
    }

    environment {
        CI = 'true'
    }

    stages {

        stage('Build') {
            steps {
                sh 'make venv'
            }
        }

        stage('Test') {
            steps {
                sh 'make check'
            }

            post {
                always {
                    junit(testResults: 'test-reports/*.xml', allowEmptyResults: true)
                    /*
                    publishHTML (target: [
                    alwaysLinkToLastBuild: false,
                    allowMissing: true,
                    keepAll: true,
                    reportDir: 'htmlcov',
                    reportFiles: '*.html',
                    reportName: "Coverage report"
                    ])
                    */
                    
                    step([$class: 'CoberturaPublisher',
                                   autoUpdateHealth: false,
                                   autoUpdateStability: false,
                                   coberturaReportFile: 'coverage.xml',
                                   failNoReports: false,
                                   failUnhealthy: false,
                                   failUnstable: false,
                                   maxNumberOfBuilds: 10,
                                   onlyStable: false,
                                   sourceEncoding: 'UTF_8',
                                   zoomCoverageChart: false])
                }
            }
        }
    }
    
    post {
    always {
      deleteDir()
    }
  }

}

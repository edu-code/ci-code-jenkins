# Задание

Вам нужно произвести настройку CI своего проекта под свой учетной записью redXX в Jenkins, работающем на тестовом сервере <http://85.192.35.28:8080/>  
Требования:

- Создайте проект `redXX`, где XX цифры вашего пользователя
- Создавать Multibranch Pipeline Job нужно в проекте `redXX`, где XX цифры вашего пользователя
- Имя Job должно быть `redXX`, где XX цифры вашего пользователя

Настраивать будем первый проект без фронтенда, который вы делали на основе <https://gitlab.com/edu-code/frameworks-boilerplate.> Если готового проекта у вас нет, берите код из ветки `done`, там мой вариант решения и переносите в свой репозиторий. Ссылка - <https://gitlab.com/edu-code/frameworks-boilerplate/tree/done>

В свой проект добавляем 2 файла из моего сделанного примера - `https://gitlab.com/edu-code/ci-code/tree/master`

- `requirements.txt` - тут у нас появились новые тестовые зависимости. Для простоты у нас тестовые и прод зависимости в одном файле.
- `Makefile` - файл настроек для утилиты make. Изначально предназначался для автоматизации компиляции  кода на языке с. Но можно использовать для любой другой автоматизации. [Подробнее](https://habr.com/ru/post/211751/)

Для начала немного теории по интеграции. Интеграция идет по 2 направлениям:

1. Клонирование исходного кода (Настраивается в Jenkins)
2. Отправка статуса прогона в GitLab (Настраивается в Jenkins)

Создаем новый Multibranch Pipeline Job в Jenkins, согласно инструкции:

<https://jenkins.io/doc/tutorials/build-a-multibranch-pipeline-project/>

<https://github.com/kitconcept/jenkins-pipeline-examples/blob/master/README.rst>

<https://mdyzma.github.io/2017/10/14/python-app-and-jenkins/>

Первое что нужно сделать это подготовить Jenkinsfile с описанием того, как буде собираться наш проект:

```groovy
pipeline {
    agent {
        docker {
            image 'python:3.7.2'
        }
    }

    options {
        // Configure an overall timeout for the build of one hour.
        timeout(time: 1, unit: 'HOURS')
        // When we have test-fails e.g. we don't need to run the remaining steps
        skipStagesAfterUnstable()
    }

    environment {
        CI = 'true'
    }

    stages {

        stage('Build') {
            steps {
                sh 'make venv'
            }
        }

        stage('Test') {
            steps {
                sh 'make check'
            }

            post {
                always {
                    junit(testResults: 'test-reports/*.xml', allowEmptyResults: true)

                    publishHTML (target: [
                    alwaysLinkToLastBuild: false,
                    allowMissing: true,
                    keepAll: true,
                    reportDir: 'htmlcov',
                    reportFiles: '*.html',
                    reportName: "Coverage report"
                    ])
                }
            }
        }
    }

}
```

Так же нам необходимо, чтобы отчет о покрытии кода тестами сохранялся после прогона как артефакт.
Для этого При после запуска тестов, необходимо опубликовать наши unit и cov репорты

Для того чтобы нельзя было принять merge request до успешного прохождения CI, нужно в настройках проекта в GitLab включить General Settings -> Merge requests -> Merge checks -> Pipelines must succeed

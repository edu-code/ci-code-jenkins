.PHONY: all lint pretty clean

all: test

check: lint test radon bandit

# We use both becase they find fixes after each other
pretty: autopep8 yapf
	
venv: requirements.txt
	python3 -m venv venv
	. venv/bin/activate && pip3 install -r requirements.txt

lint: venv
	. venv/bin/activate && flake8

test: venv
	. venv/bin/activate && \
	coverage run --branch --omit="venv/*,tests/*,tests.py" tests.py && \
	coverage report && \
	coverage xml --fail-under=70 && \
	coverage erase

autopep8: venv
	. venv/bin/activate && \
	# TODO: add venv/* and migrations/* exclusion
	autopep8 --in-place --aggressive --aggressive -r .

yapf: venv
	. venv/bin/activate && \
	# TODO: -e "venv/*|migrations/*" not working
	yapf -ir -e "venv/*|migrations/*" .

radon: venv
	. venv/bin/activate && \
	radon cc -s -a --ignore='venv,migrations' . && \
	radon mi -s --ignore='venv,migrations' .

bandit: venv
	. venv/bin/activate && \
	bandit -r --exclude='./venv/,./migrations/' .

clean:
	rm -rf venv
	rm -rf htmlcov
	find -iname "*.pyc" -delete
	find -type d -name __pycache__ -delete
